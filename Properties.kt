
// 1) "Properties"

class PropertyExample() {
    var counter = 0
    var propertyWithCounter: Int? = null
        set(value) {
            field = value
            counter++
        }
}

// 2) "Lazy properties"

class LazyProperty(val initializer: () -> Int) {
    var lazyValue: Int? = null
    val lazy: Int
        get() {
            if (lazyValue != null) return lazyValue!!
            lazyValue = initializer()
            return lazyValue!!
        }
}

// 3) "Delegates examples"

class LazyProperty(val initializer: () -> Int) {
    val lazyValue: Int by lazy { initializer() }
}

// 4) "Delegates how it works"

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class D {
    var date: MyDate by EffectiveDate()
}

class EffectiveDate<R> : ReadWriteProperty<R, MyDate> {

    var timeInMillis: Long? = null

    override fun getValue(thisRef: R, property: KProperty<*>): MyDate {
        return timeInMillis!!.toDate()
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: MyDate) {
        timeInMillis = value.toMillis()
    }
}
